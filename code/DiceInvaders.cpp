#include <windows.h>
#include "DiceInvaders.h"
#include <cassert>
#include <stdio.h>

class DiceInvadersLib
{
public:
	explicit DiceInvadersLib(const char* libraryPath)
	{
		m_lib = LoadLibrary(libraryPath);
		assert(m_lib);

		DiceInvadersFactoryType* factory = (DiceInvadersFactoryType*)GetProcAddress(
				m_lib, "DiceInvadersFactory");
		m_interface = factory();
		assert(m_interface);
	}

	~DiceInvadersLib()
	{
		FreeLibrary(m_lib);
	}

	IDiceInvaders* get() const 
	{
		return m_interface;
	}

private: 
	DiceInvadersLib(const DiceInvadersLib&);
	DiceInvadersLib& operator=(const DiceInvadersLib&);

private: 
	IDiceInvaders* m_interface;
	HMODULE m_lib;
};

static 
void initialize(Alien *aliens,int rowAliens,int colAliens){
	for(int row = 0; row<rowAliens;++row)
	{
		for(int col = 0; col <colAliens;++col)
		{
			aliens[row*colAliens+col].x = 32.0f*2*col;
			aliens[row*colAliens+col].y = 32.0f+48.0f*row;
			aliens[row*colAliens+col].alive = true;
		}
	}
}

int APIENTRY WinMain(
	HINSTANCE instance,
	HINSTANCE previousInstance,
	LPSTR commandLine,
	int commandShow)
{
	DiceInvadersLib lib("DiceInvaders.dll");
	IDiceInvaders* system = lib.get();

	const int screenX = 640;
	const int screenY = 480;
	const int spriteSize = 32;
	system->init(screenX, screenY);

	ISprite* playerSprite = system->createSprite("data/player.bmp");
	ISprite* enemy1Sprite = system->createSprite("data/enemy1.bmp");
	ISprite* enemy2Sprite = system->createSprite("data/enemy2.bmp");
	ISprite* rocketSprite = system->createSprite("data/rocket.bmp");
	ISprite* bombSprite   = system->createSprite("data/bomb.bmp");
	float horizontalPosition = 320.0f;
	int lifes = 3;
	int points = 0;


	const int nbrRockets = 6;
	int activeRockets = 0;
	float rocketRate = 0.5f;
	float lastRocketFire = 0.0f;
	v2 rocketPos[nbrRockets] = {};
	
	const int rowAliens = 5;
	const int colAliens = 4;

	Alien aliens[rowAliens*colAliens];
	initialize(aliens,rowAliens,colAliens);

	float alienSpeed = 1.0f;	
	const int maxBombs = 20;
	v2 bombPos[20] = {};
	int activeBombs = 0;
	srand(system->getElapsedTime());
	float bombSpawnTime = 0.0f;
	float lastTime = system->getElapsedTime();
	char textBuff[32];
	while (system->update())
	{
		float newTime = system->getElapsedTime();
		float moveMultiplier = (newTime - lastTime) * 160.0f;
		
		IDiceInvaders::KeyStatus keys;
		system->getKeyStatus(keys);
		if (keys.right&&horizontalPosition + moveMultiplier<float(screenX-spriteSize))
			horizontalPosition += moveMultiplier;
		else if (keys.left&&horizontalPosition - moveMultiplier>0)
			horizontalPosition -= moveMultiplier;
		if(keys.fire && newTime>lastRocketFire + rocketRate && activeRockets < nbrRockets)
		{
			lastRocketFire = newTime;
			rocketPos[activeRockets].x = horizontalPosition;
			rocketPos[activeRockets].y = screenY-2.0f*spriteSize;
			++activeRockets;
		}

		for(int rIndex = 0; rIndex < activeRockets; ++rIndex)
		{
			rocketPos[rIndex].y -= moveMultiplier;
			if(rocketPos[rIndex].y < -spriteSize)
			{//last rocket in array is positioned at current index and drawn arraysize decreased
				--activeRockets;
				rocketPos[rIndex] = rocketPos[activeRockets];
			}
		}

		float alienSpeedY = 0.0f;
		for(int aIndex = 0; aIndex<rowAliens*colAliens;++aIndex)
		{
			if(aliens[aIndex].alive&&
				((alienSpeed>0.0f &&aliens[aIndex].x>screenX-spriteSize)||
				(alienSpeed<0.0f &&aliens[aIndex].x<0.0f)))
			{
				alienSpeedY = float(spriteSize);
				alienSpeed = -alienSpeed;
				break;
			}
		}

		for(int aIndex = 0; aIndex<rowAliens*colAliens;++aIndex)
		{
				aliens[aIndex].x += alienSpeed*moveMultiplier;//horizontal mov
				aliens[aIndex].y += alienSpeedY;//vertical move
		}

		//DropBombs
		if(activeBombs<maxBombs&& bombSpawnTime+0.1f<newTime)
		{
			bombSpawnTime = newTime;
			if(rand()%5==1)
			{
				int aIndex = rand()%(colAliens*rowAliens);
				if(aliens[aIndex].alive){
					bombPos[activeBombs].x = aliens[aIndex].x;
					bombPos[activeBombs].y = aliens[aIndex].y+float(spriteSize);
					++activeBombs;
				}
			}
		}

		for(int bIndex = 0; bIndex < activeBombs; ++bIndex)
		{
			bombPos[bIndex].y += moveMultiplier;
			if(float(screenY)<bombPos[bIndex].y)
			{//last Bomb in array is positioned at current index and drawn arraysize decreased
				--activeBombs;
				bombPos[bIndex] = bombPos[activeBombs];
			}
		}

		
		//intersectiontest with player
		for(int bIndex = 0; bIndex < activeBombs; ++ bIndex)
		{
			if(bombPos[bIndex].y>= float(screenY-spriteSize)// discardBombs above player head
				&&bombPos[bIndex].x+spriteSize/2<=float(horizontalPosition+spriteSize) 
				&&bombPos[bIndex].x+spriteSize/2>=float(horizontalPosition))
			{
				--lifes;
				--activeBombs;
				bombPos[bIndex] = bombPos[activeBombs];
			}
		}
		for(int row = rowAliens-1; row>=0;--row)
		{
			if(aliens[row*colAliens+0].y< float(screenY -2*spriteSize))
			{//if row has not reached player exit loop
				break;
			}
			for(int col = 0; col<colAliens;++col)
			{
				int index = row*colAliens+col;
				if(aliens[index].alive&&(
					(aliens[index].x<=horizontalPosition+spriteSize&&
					aliens[index].x >=horizontalPosition)||(
					aliens[index].x+spriteSize<=horizontalPosition+spriteSize&&
					aliens[index].x+spriteSize >=horizontalPosition)
					))
				{//alien hit player
					--lifes;
					aliens[index].alive = false;
				}
				else if(aliens[index].alive && aliens[index].y + spriteSize>= float(screenY))
				{
					--lifes;
					aliens[index].alive = false;
				}
			}
		}

		//intersection rocketAliens
		for(int rIndex = 0; rIndex < activeRockets;++rIndex){
			float rocketX = rocketPos[rIndex].x+spriteSize/2;
			float rocketYUp =rocketPos[rIndex].y; 
			if((
				(rocketX>=aliens[0].x)&&
				(rocketX<=aliens[colAliens-1].x+spriteSize)
				)
				&&(
				(rocketYUp>=aliens[0].y)&&
				(rocketYUp<=aliens[(rowAliens-1)*colAliens+(colAliens-1)].y+spriteSize)
				))
			{
				for(int aIndex = 0; aIndex<rowAliens*colAliens;++aIndex)
				{
					if((aliens[aIndex].alive) &&
						(rocketX<=aliens[aIndex].x+spriteSize&&
						rocketX>=aliens[aIndex].x)&&
						(
							(rocketYUp<=aliens[aIndex].y+spriteSize&&
								rocketYUp>=aliens[aIndex].y)||
							(rocketYUp+spriteSize <= aliens[aIndex].y+spriteSize&&
								rocketYUp+spriteSize >= aliens[aIndex].y
							)
						)
					)
					{
						aliens[aIndex].alive = false;
						--activeRockets;
						rocketPos[rIndex].x = rocketPos[activeRockets].x;
						rocketPos[rIndex].y = rocketPos[activeRockets].y;
						points += 50;
					}
				}
			}
		}

		//Check lvl cleared;
		bool aliensAlive = false;
		for(int alienIndex = 0; alienIndex < rowAliens+colAliens; ++alienIndex)
		{
			if(aliens[alienIndex].alive){
				aliensAlive = true;
				break;
			}
		}
		if(!aliensAlive){
			initialize(aliens,rowAliens,colAliens);
			alienSpeed *= 1.15f;
		}
		
		while(lifes<1&&system->update()){
			//game over loop
			sprintf(textBuff,"Game Over");
			system->drawText(screenX/2,screenY/2+8,textBuff);
			
			sprintf(textBuff,"Points: %d", points);
			system->drawText(screenX/2,screenY/2+32,textBuff);

		
			IDiceInvaders::KeyStatus keys;
			system->getKeyStatus(keys);
			if(!keys.fire)
				activeRockets = -1;
			if(keys.fire && activeRockets == -1)
			{
				lifes = 3;
				points = 0;
				alienSpeed = 1.0f;
				horizontalPosition = 320.0f;
				activeRockets = 0;
				activeBombs = 0;

				initialize(aliens,rowAliens,colAliens);
				break;
			}
		}

		//render
		playerSprite->draw(int(horizontalPosition), screenY-spriteSize);
		for(int i = 1; i<= lifes;++i)
		{
			playerSprite->draw(screenX-60-48*i, 0);	
		}
		sprintf(textBuff,"Points: %d", points);
		system->drawText(8,8,textBuff);
		sprintf(textBuff,"Lifes: %d", lifes);
		system->drawText(8,32,textBuff);
		
		for(int rIndex = 0; rIndex < activeRockets; ++rIndex)
		{
			rocketSprite->draw(int(rocketPos[rIndex].x), int(rocketPos[rIndex].y));
		}

		for(int aIndex = 0; aIndex<rowAliens*colAliens;++aIndex)
		{
			if(aliens[aIndex].alive){
				if(aIndex<colAliens*2)
					enemy1Sprite->draw(aliens[aIndex].x,aliens[aIndex].y);
				else
					enemy2Sprite->draw(aliens[aIndex].x,aliens[aIndex].y);
			}
		}

		for(int bIndex = 0; bIndex < activeBombs; ++bIndex)
		{
			bombSprite->draw(int(bombPos[bIndex].x), int(bombPos[bIndex].y));
		}
		
		lastTime = newTime;
	}

	playerSprite->destroy();
	system->destroy();
	return 0;
}



